install.packages("deldir")
library(deldir)

#draws a patch of a random tiling

#range
r1<-20
#density
d1<-15

#generate random numbers
xran<-runif(r1*d1,0,r1)
yran<-runif(r1*d1,0,r1)

#Plot a tesselation of our random point pattern
deldir(xran,yran,plotit = TRUE,wlines = "tess",wpoints = "none",lty=c(1,1))