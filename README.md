# ProjectionTilingscodim1
This includes python scripts which generate point patterns of dimensions one, two and three. 
With additonal R scripts used to triangulate these point patterns and draw the associated tilings.

Along with another R script which was used generate a patch of a tiling based on uniform random numbers.