import math
p1=11.0
p2=7.0

intercept = math.pi
def projF(x,y,z):
    return (x+y*(p1**0.5)+z*(p2**0.5))/(1+p1+p2)

def projE(x,y,z):
    change= ( intercept - x -y*(p1**0.5) - z*(p2**0.5))/(1+p1+p2)
    a= x + change
    b= y + change*(p1**0.5)
    c= z + change*(p2**0.5)
    return [a,b,c]
#cup = point in boundary of acceptance domain
cup = projF(13**0.5,1,1)
ptiles = []
tiles=[]
tilesx=[]
tilesy=[]
tilesz=[]
def proN(r):
    for i in range(-25,30):
        for j in range(-25,30):
            for w in range(-25,30):
                if 0 < projF(i,j,w) < cup:
                    tilesx.append(projE(i,j,w)[0])
                    tilesy.append(projE(i,j,w)[1])
                    tilesz.append(projE(i,j,w)[2])
                    print "working ....",len(tilesx)
                    if len(tilesx)== r:
                       print "X        ",tilesx
                       print "Y        ",tilesy
                       print "Z        ",tilesz
                       return " "


proN(1200)